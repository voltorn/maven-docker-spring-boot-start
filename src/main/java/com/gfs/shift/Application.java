package adrian.work.maven.docker;

import adrian.work.maven.docker.config.JpaDatasourceConfiguration;
import adrian.work.maven.docker.config.RestApiConfiguration;
import adrian.work.maven.docker.config.SwaggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Import({ JpaDatasourceConfiguration.class, SwaggerConfig.class, RestApiConfiguration.class})
@SpringBootApplication(scanBasePackages={"adrian.work.maven.docker"})
@RestControllerAdvice
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
}
