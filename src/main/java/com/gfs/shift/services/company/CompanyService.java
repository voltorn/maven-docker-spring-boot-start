package adrian.work.maven.docker.services.company;

import adrian.work.maven.docker.base.BaseCrudService;
import org.springframework.stereotype.Service;

@Service
public class CompanyService extends BaseCrudService<CompanyModel, CompanyEntity, Long>  {

    public CompanyService() {
        super(CompanyModel.class, CompanyEntity.class);
    }

}
