package adrian.work.maven.docker.services.account;

import adrian.work.maven.docker.base.BaseCrudService;
import org.springframework.stereotype.Service;

@Service
public class AccountService extends BaseCrudService<AccountModel, AccountEntity, Long>  {

    public AccountService() {
        super(AccountModel.class, AccountEntity.class);
    }

}
