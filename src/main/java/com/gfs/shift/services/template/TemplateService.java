package adrian.work.maven.docker.services.template;

import adrian.work.maven.docker.services.template.TemplateEntity;
import adrian.work.maven.docker.base.BaseCrudService;
import org.springframework.stereotype.Service;

@Service
public class TemplateService extends BaseCrudService<TemplateModel, TemplateEntity, Long> {

    public TemplateService() {
        super(TemplateModel.class, TemplateEntity.class);
    }

}
