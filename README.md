# OCP Maven Docker Super Awesome Cowboy  README

**Launch Spring Boot App Docker - Linked to adjacent MySQL container**

```shell

git clone https://bitbucket.org/voltorn/maven-docker-spring-boot-start.git

cd maven-docker*

mvn clean install docker:run

```


** If Docker won't fire, check the POM**

Make sure this is false - nice for debugging - but confusing for new folk.

```xml

<docker.skip>false</docker.skip>

```